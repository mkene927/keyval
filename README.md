# Welcome

Welcome to this KeyVal Wiki Application. The following should explain a little about the application and how the development progress notes. 

## Purpose for Applicatoin

One of the programs I have to log into every morning to trade with 
requires extra authentication in the form of referencing a security 
card.  Basically I have to log in with user name a password, then the 
program will pull up a set of two numbers, I then have to reference 
these two numbers from a security card that I have and put the 
corresponding sequence of numbers and letters into the program, then it 
finishes the authentication and logs me in.  Needless to say it is very 
tedious.
I'm hoping to get a simple app or windows based program that will do 
three things.

1.  Allow me to create a very simple database
2.  Allow me to reference that database by being able to type in two 
different numbers
3.  Copy the corresponding numbers to the clipboard, which I can then 
paste into my program.
 
For Example:  Say the following set in the database
 
1. qwe
2.  2f4
3.  4n9
4.  9sf
etc.
 
Say the program asks for 1 and 3... I would then type 1 and 3 into the 
app and it would then return the values qwe and 4n9 and automatically 
copy qwe4n9 to the clipboard.

## Major Release Notes
[Beta 1.0 Release](https://bitbucket.org/mkene927/keyval/wiki/Beta%201.0%20Release%20Log)

## Wiki Issue Tracker (Fixed. See Trello details below)
[Known Issues](https://bitbucket.org/mkene927/keyval/wiki/Known%20Issues)

## Trello Issue Tracking

Issue Tracking for this project is publicaally setup on Trello: [Trello Link](https://trello.com/b/5P8x8DLU/keyval-app-dev)